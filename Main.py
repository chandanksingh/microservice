from flask import Flask, render_template
import socket
import json

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template("home.html")

@app.route("/myip")
def myip():
    hostname = socket.gethostname()
    ip_address =socket.gethostbyname(hostname)
    resp = { 'ip' : ip_address}
    response = json.dumps(resp)
    
    return render_template("myip.html", resp=response)

if __name__ == "__main__":
    app.run()